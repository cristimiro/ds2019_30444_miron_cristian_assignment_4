package com.ds.Assignment1.config;

import com.ds.Assignment1.services.ActivityService;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Component
public class RabbitMqConfig {


    private final static String QUEUE_NAME = "hello";
    private static JSONParser jsonParser = new JSONParser();
//    private static Gson gson = new Gson();

    @Autowired
    private ActivityService activityService;

	@Bean
	public Channel createStupidConnect() throws TimeoutException, IOException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody());
			try {
				JSONObject jsonObject = (JSONObject) jsonParser.parse(message);
				activityService.patientActivities(jsonObject);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		};
		channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });

		return channel;
	}
}
