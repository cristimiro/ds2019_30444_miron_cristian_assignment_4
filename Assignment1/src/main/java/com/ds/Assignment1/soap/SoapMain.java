package com.ds.Assignment1.soap;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import javax.xml.stream.XMLStreamException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoapMain extends JFrame {

    private int sleepingCounter = 0;
    private int toiletingCounter = 0;
    private int showeringCounter = 0;
    private int breakfastCounter = 0;
    private int groomingCounter = 0;
    private int spare_timeCounter = 0;
    private int leavingCounter = 0;
    private int lunchCounter = 0;
    private int snackCounter = 0;
    private boolean isTaken = false;
    private static final long serialVersionUID = 1L;

    private JButton medPlansButton = new JButton("Get Medication Plan");
    private JLabel medPlanId = new JLabel("Enter Id");
    private JTextField medPlanIdField = new JTextField();
    private JLabel receivedText = new JLabel();


    public SoapMain(){
        initChartUI();
    }

    private void initChartUI(){
        try {
            sendRequest(true, 0);
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
        CategoryDataset dataset = createDataset();

        JFreeChart chart = createChart(dataset);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.white);
        add(chartPanel);

        pack();
        setTitle("Bar chart");

        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        chartPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    initMedPlanUI();
                } catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

    }


    private void initMedPlanUI() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        setSize(600, 450);
        setTitle("Medication Plan");
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(medPlanId);
        panel.add(medPlanIdField);
        panel.add(medPlansButton);
        panel.add(receivedText);

        medPlanId.setBounds(100,50,80,20);
        medPlanIdField.setBounds(100, 80, 40,20);
        medPlansButton.setBounds(80,110,150,40);
        receivedText.setBounds(20, 160, 550,200);

        medPlansButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sendRequest(false, Integer.parseInt(medPlanIdField.getText()));
                } catch (IOException | XMLStreamException e1) {
                    e1.printStackTrace();
                }
                medPlanTakenMethod(receivedText);
                isTaken = false;
            }
        });
        this.setContentPane(panel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void medPlanTakenMethod(JLabel jLabel) {
        if(isTaken){
            jLabel.setText("Taken");
        }else{
            jLabel.setText("Not Taken");
        }
    }

    private JFreeChart createChart(CategoryDataset dataset) {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Activities Counter",
                "",
                "Number of",
                dataset,
                PlotOrientation.VERTICAL,
                false, true, false);
        return barChart;
    }

    private CategoryDataset createDataset(){
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.setValue(this.sleepingCounter, "Activities", "Sleeping");
        dataset.setValue(this.toiletingCounter, "Activities", "Toileting");
        dataset.setValue(this.showeringCounter, "Activities", "Showering");
        dataset.setValue(this.breakfastCounter, "Activities", "Breakfast");
        dataset.setValue(this.groomingCounter, "Activities", "Grooming");
        dataset.setValue(this.spare_timeCounter, "Activities", "SpareTime");
        dataset.setValue(this.leavingCounter, "Activities", "Leaving");
        dataset.setValue(this.lunchCounter, "Activities", "Lunch");
        dataset.setValue(this.snackCounter, "Activities", "Snack");
        return dataset;
    }

    public void sendRequest(boolean check, int id) throws IOException, XMLStreamException {
        String url = "http://localhost:8080/ws";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        String myRequest = "";
        if (check) {
            myRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                    "xmlns:med=\"http://www.ds.com/Assignment1/soap\">\n" +
                    "    <soapenv:Header/>\n" +
                    "    <soapenv:Body>\n" +
                    "        <med:DoctorRequest>\n" +
                    "            <med:patientId/>\n" +
                    "            <med:activityRecommendationId/>\n" +
                    "            <med:medicalPlanId/>\n" +
                    "            <med:recommendation/>\n" +
                    "        </med:DoctorRequest>\n" +
                    "    </soapenv:Body>\n" +
                    "</soapenv:Envelope>";
        } else {
            myRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                    "xmlns:med=\"http://www.ds.com/Assignment1/soap\">\n" +
                    "    <soapenv:Header/>\n" +
                    "    <soapenv:Body>\n" +
                    "        <med:DoctorRequest>\n" +
                    "            <med:patientId/>\n" +
                    "            <med:activityRecommendationId/>\n" +
                    "            <med:medicalPlanId>"+ id +"</med:medicalPlanId>\n" +
                    "            <med:recommendation/>\n" +
                    "        </med:DoctorRequest>\n" +
                    "    </soapenv:Body>\n" +
                    "</soapenv:Envelope>";
        }
        con.setRequestProperty("Content-Type", "text/xml");
        con.setDoOutput(true);
        con.getOutputStream().write(myRequest.getBytes("UTF8"));
        int responseCode = con.getResponseCode();
        System.out.println("Response Code: "+responseCode);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while((inputLine = in.readLine()) != null){
            response.append(inputLine);
        }
        in.close();
        if (check){
            Pattern activityPattern = Pattern.compile("<ns2:(id|name|user_id|startTime|endTime|status)>(.*?)\\<\\/ns2:(id|name|user_id|startTime|endTime|status)>");
            Set<ActivityType> activityTypes =new HashSet<>();

            Matcher matcher = activityPattern.matcher(response);
            ActivityType activityType = new ActivityType();
            while(matcher.find()){
                if(matcher.group(1).equals("id")){
                    activityType.setId(matcher.group(2));
                }
                if(matcher.group(1).equals("name")){
                    activityType.setName(matcher.group(2));
                }
                if(matcher.group(1).equals("user_id")){
                    activityType.setUserId(matcher.group(2));
                }
                if(matcher.group(1).equals("startTime")){
                    activityType.setStartTime(matcher.group(2));
                }
                if(matcher.group(1).equals("endTime")){
                    activityType.setEndTime(matcher.group(2));
                }
                if(matcher.group(1).equals("status")){
                    activityType.setStatus(matcher.group(2));
                }
                if(activityType.getStatus()!=null){
                    if(activityType.getTimeSpent() == null){
                        activityType.setTimeSpent("0");
                    }
                    activityTypes.add(activityType);
                    if(activityType.getStatus().equals("not ok") && activityType.getName().equals("sleeping") && Integer.parseInt(activityType.getTimeSpent()) > 18){
                        System.out.println("Patient must be checked. More than 18 hours of sleep!");
                    } else if(activityType.getStatus().equals("not ok") && activityType.getName().equals("leaving") && Integer.parseInt(activityType.getTimeSpent()) > 18){
                        System.out.println("Patient must be checked. More than 18 hours since he left home!");
                    }else if(activityType.getStatus().equals("not ok") && activityType.getName().equals("toileting")){
                        System.out.println("Patient must be checked. More than 18 hours of sleep!");
                    } else{
                        System.out.println("Everithing is fine!");
                    }
                    activityType = new ActivityType();
                }
            }
            for(ActivityType activityType1 : activityTypes){
                if(activityType1.getName().contains("sleeping")){
                    this.sleepingCounter++;
                }else if(activityType1.getName().contains("toileting")){
                    this.toiletingCounter++;
                }else if(activityType1.getName().contains("showering")){
                    this.showeringCounter++;
                }else if(activityType1.getName().contains("breakfast")){
                    this.breakfastCounter++;
                }else if(activityType1.getName().contains("grooming")){
                    this.groomingCounter++;
                }else if(activityType1.getName().contains("spare_time")){
                    this.spare_timeCounter++;
                }else if(activityType1.getName().contains("leaving")){
                    this.leavingCounter++;
                }else if(activityType1.getName().contains("lunch")){
                    this.lunchCounter++;
                }else if(activityType1.getName().equals("snack")){
                    this.snackCounter++;
                }
            }
        } else{
            Pattern medPlanPattern = Pattern.compile("<ns2:(id|end_date|start_date|period|status)>(.*?)\\<\\/ns2:(id|end_date|start_date|period|status)>");
            MedicationPlanType medicationPlanType = new MedicationPlanType();
            Matcher matcher = medPlanPattern.matcher(response);
            try{
                while(matcher.find()){
                    if(matcher.group(1).equals("id")){
                        medicationPlanType.setId(matcher.group(2));
                    }
                    if(matcher.group(1).equals("end_date")){
                        medicationPlanType.setEndDate(matcher.group(2));
                    }
                    if(matcher.group(1).equals("start_date")){
                        medicationPlanType.setStartDate(matcher.group(2));
                    }
                    if(matcher.group(1).equals("period")){
                        medicationPlanType.setPeriod(matcher.group(2));
                    }
                    if(matcher.group(1).equals("status")){
                        medicationPlanType.setStatus(matcher.group(2));
                    }
                }
                if(medicationPlanType.getStatus().contains("not taken")){
                    if(!isTaken) {
                        this.isTaken = false;
                    }
                } else{
                    this.isTaken = true;
                }
            }catch(NullPointerException e){
                this.isTaken = false;
            }

        }
    }

    @Override
    public String toString() {
        return "SoapMain{" +
                "sleepingCounter=" + sleepingCounter +
                ", toiletingCounter=" + toiletingCounter +
                ", showeringCounter=" + showeringCounter +
                ", breakfastCounter=" + breakfastCounter +
                ", groomingCounter=" + groomingCounter +
                ", spare_timeCounter=" + spare_timeCounter +
                ", leavingCounter=" + leavingCounter +
                ", lunchCounter=" + lunchCounter +
                ", snackCounter=" + snackCounter +
                '}';
    }

    public static void main(String[] args) throws IOException {
        SwingUtilities.invokeLater(() -> {
            SoapMain ex = new SoapMain();
            ex.setVisible(true);
        });
    }

}
