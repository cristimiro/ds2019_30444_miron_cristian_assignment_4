package com.ds.Assignment1.endpoints;

import com.ds.Assignment1.services.MyService;
import com.ds.Assignment1.soap.DoctorRequest;
import com.ds.Assignment1.soap.DoctorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class AllActivitiesEndpoint {

    private static final String NAMESPACE = "http://www.ds.com/Assignment1/soap";

    @Autowired
    private MyService myService;

    @PayloadRoot(namespace = NAMESPACE,localPart = "DoctorRequest")
    @ResponsePayload
    public DoctorResponse getDoctorResponse(@RequestPayload DoctorRequest doctorRequest){
        return myService.getAllRequests(doctorRequest);
    }

}
