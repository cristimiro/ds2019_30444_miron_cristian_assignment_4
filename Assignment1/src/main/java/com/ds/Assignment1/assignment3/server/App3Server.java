package com.ds.Assignment1.assignment3.server;

import com.ds.Assignment1.assignment3.common.MedPlanServiceImpl;
import com.ds.Assignment1.services.MedicationPlanService;
import com.ds.Assignment1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.rmi.UnknownHostException;

@Configuration
public class App3Server {

    @Autowired
    private MedicationPlanService medicationPlanService;

    @Bean
    public MedPlanService medPlanService(){
        return new MedPlanServiceImpl(medicationPlanService);
    }

    @Bean
    public RmiServiceExporter exporter() throws UnknownHostException {
        RmiServiceExporter rse = new RmiServiceExporter();
        rse.setServiceName("OrderService");
        rse.setService(medPlanService());
        rse.setServiceInterface(MedPlanService.class);
        rse.setRegistryPort(2099);
        return rse;
    }
}
