package com.ds.Assignment1.assignment3.common;

import com.ds.Assignment1.assignment3.server.MedPlanService;
import com.ds.Assignment1.entities.MedicationPlan;
import com.ds.Assignment1.services.MedicationPlanService;

public class MedPlanServiceImpl implements MedPlanService {

    private final MedicationPlanService medicationPlanService;

    public MedPlanServiceImpl(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @Override
    public MedicationPlan getMedicationPlans(int id) {
        MedicationPlan medicationPlan = this.medicationPlanService.getById(id);
//        System.out.println(medicationPlan.toString());
//        Patient patient = this.patientService.getById(id);
        return medicationPlan;
    }
}
