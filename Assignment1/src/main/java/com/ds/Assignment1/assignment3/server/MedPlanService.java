package com.ds.Assignment1.assignment3.server;

import com.ds.Assignment1.entities.MedicationPlan;

public interface MedPlanService {

    MedicationPlan getMedicationPlans(int id);

}
