package com.ds.Assignment1.assignment3.client;

import com.ds.Assignment1.assignment3.server.MedPlanService;
import com.ds.Assignment1.entities.MedicationPlan;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class MedPlanBean {
    @Autowired
    private MedPlanService medPlanService;

    public String getAllMedPlans(int id){
        System.out.println("getting medication plans :)");
        System.out.println(medPlanService.getMedicationPlans(1));
        return medPlanService.getMedicationPlans(id).toString();
    }
}
