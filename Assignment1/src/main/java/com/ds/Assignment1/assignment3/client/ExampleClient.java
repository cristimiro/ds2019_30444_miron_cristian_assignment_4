package com.ds.Assignment1.assignment3.client;

import com.ds.Assignment1.assignment3.server.MedPlanService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.TimerTask;

public class ExampleClient extends JFrame {

    @Bean
    public MedPlanBean medPlanBean(){
        return new MedPlanBean();
    }
    @Bean
    public RmiProxyFactoryBean exporter() throws UnknownHostException {
        RmiProxyFactoryBean rpfb = new RmiProxyFactoryBean();
        rpfb.setServiceInterface(MedPlanService.class);
        String hostAddress = Inet4Address.getLocalHost()
                .getHostAddress();
        rpfb.setServiceUrl(String.format("rmi://%s:2099/OrderService", hostAddress));
        return rpfb;
    }


    private static final long serialVersionUID = 1L;
    private static AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ExampleClient.class);

    private JButton medPlansButton = new JButton("Get Medication Plan");
    private JLabel medPlanId = new JLabel("Enter Id");
    private JTextField medPlanIdField = new JTextField();
    private JLabel receivedText = new JLabel();
    private JLabel dateAndTime = new JLabel();


    public ExampleClient() throws ClassNotFoundException, InstantiationException,
            IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        setSize(600, 450);
        setTitle("Medication Plan");
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(medPlanId);
        panel.add(medPlanIdField);
        panel.add(medPlansButton);
        panel.add(receivedText);
        panel.add(dateAndTime);
        medPlanId.setBounds(100,50,80,20);
        medPlanIdField.setBounds(100, 80, 40,20);
        medPlansButton.setBounds(80,110,150,40);
        receivedText.setBounds(20, 160, 550,200);
        dateAndTime.setBounds(10, 10 ,200, 20);

        medPlansButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myMethod(receivedText, medPlanIdField);
            }
        });

        ActionListener updateTime = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getDateAndTime().setText(new Date().toString());
            }
        };

        Timer t = new Timer(1000, updateTime);
        t.start();

        java.util.Timer t2 = new java.util.Timer();
        t2.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    writeFile(medPlanIdField);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                System.out.println("This will run every 5 seconds");

            }
        }, 5000, 5000);

        this.setContentPane(panel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public JLabel getDateAndTime(){
        return this.dateAndTime;
    }

    public void writeFile(JTextField id) throws FileNotFoundException {
        MedPlanBean bean = context.getBean(MedPlanBean.class);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter("output.txt"));
            out.write(bean.getAllMedPlans(Integer.parseInt(id.getText())));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void myMethod(JLabel jLabel, JTextField id) {
        MedPlanBean bean = context.getBean(MedPlanBean.class);
        jLabel.setText(bean.getAllMedPlans(Integer.parseInt(id.getText())));
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        ExampleClient exampleClient = new ExampleClient();
        exampleClient.setVisible(true);
    }

}
