package com.ds.Assignment1.repositories;


import com.ds.Assignment1.entities.Medication;
import com.ds.Assignment1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {
    @Query(value = "SELECT u " +
            "FROM Medication u " +
            "ORDER BY u.id")
    List<Medication> getAllOrdered();
}
