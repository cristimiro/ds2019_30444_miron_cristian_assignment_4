package com.ds.Assignment1.repositories;

import com.ds.Assignment1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
