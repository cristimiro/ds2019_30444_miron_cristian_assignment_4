package com.ds.Assignment1.repositories;

import com.ds.Assignment1.entities.SideEffect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SideEffectRepository extends JpaRepository<SideEffect, Integer> {
    @Query(value = "SELECT u " +
            "FROM SideEffect u " +
            "ORDER BY u.id")
    List<SideEffect> getAllOrdered();
}
