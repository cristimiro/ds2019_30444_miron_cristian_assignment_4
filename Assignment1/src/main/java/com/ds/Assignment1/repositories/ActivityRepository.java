package com.ds.Assignment1.repositories;

import com.ds.Assignment1.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository  extends JpaRepository<Activity, Integer> {
}
