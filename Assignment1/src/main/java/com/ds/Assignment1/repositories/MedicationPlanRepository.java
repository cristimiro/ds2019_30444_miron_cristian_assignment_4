package com.ds.Assignment1.repositories;

import com.ds.Assignment1.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

}
