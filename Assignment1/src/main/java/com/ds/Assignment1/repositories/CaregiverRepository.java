package com.ds.Assignment1.repositories;

import com.ds.Assignment1.dto.CaregiverDTO;
import com.ds.Assignment1.entities.Caregiver;
import com.ds.Assignment1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer>{
    @Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.id")
    List<Caregiver> getAllOrdered();
}
