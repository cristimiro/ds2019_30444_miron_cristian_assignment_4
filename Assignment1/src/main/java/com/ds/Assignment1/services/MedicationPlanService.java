package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.MedicationPlanDTO;
import com.ds.Assignment1.dto.builders.MedicationPlanBuilder;
import com.ds.Assignment1.entities.Medication;
import com.ds.Assignment1.entities.MedicationPlan;
import com.ds.Assignment1.repositories.MedicationPlanRepository;
import com.ds.Assignment1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
    }

    public MedicationPlanDTO findUserById(Integer id) {
        Optional<MedicationPlan> medication = medicationPlanRepository.findById(id);
        return MedicationPlanBuilder.generateDTOFromEntity(medication.get());
    }

    public MedicationPlan getById(Integer id) {
        Optional<MedicationPlan> medication = medicationPlanRepository.findById(id);
        MedicationPlan medicationPlan = medication.get();
        return medicationPlan;
    }

    public List<MedicationPlanDTO> findAll() {
        List<MedicationPlan> medications = medicationPlanRepository.findAll();
        return medications.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository
                .save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO))
                .getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public void insert(MedicationPlan medicationPlan){
        medicationPlanRepository.save(medicationPlan);
    }

    public void mapMedicine(Map<String, String> data) throws ParseException {
        Integer mid = 0;
        Integer period = 0;
        Date star_date = null;
        Date end_date = null;
        for(Map.Entry e : data.entrySet()){
            if (e.getKey().equals("period")){
                period = Integer.parseInt((String) e.getValue());
            }
            if (e.getKey().equals("m_id")){
                mid = Integer.parseInt((String) e.getValue());
            }
            if (e.getKey().equals("start_date")){
                star_date = new SimpleDateFormat("dd/MM/yyyy").parse((String) e.getValue());
            }
            if (e.getKey().equals("end_date")){
                end_date = new SimpleDateFormat("dd/MM/yyyy").parse((String) e.getValue());
            }
        }

        System.out.println("care: " + period + " " + mid + " " + star_date + " " + end_date);

        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setPeriod(period);
        medicationPlan.setStart_date(star_date);
        medicationPlan.setEnd_date(end_date);

        Optional<Medication> medicationOptional = medicationRepository.findById(mid);
        Medication medication = medicationOptional.get();
        Set<Medication> mSet = new HashSet<>();
        mSet.add(medication);
        medicationPlan.setMedicationSet(mSet);
        this.insert(medicationPlan);
    }

    public void delete(Integer id) {
        this.medicationPlanRepository.deleteById(id);
    }
}
