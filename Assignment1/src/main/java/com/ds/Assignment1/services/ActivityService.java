package com.ds.Assignment1.services;


import com.ds.Assignment1.entities.Activity;
import com.ds.Assignment1.repositories.ActivityRepository;
import com.rabbitmq.client.Channel;
//import org.json.simple.JSONObject;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;


@Service
public class ActivityService {
    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public void insert(Activity activity) {
        activityRepository.save(activity);
    }

    public void patientActivities(JSONObject jsonObject) {
        long startTime = TimeUnit.MILLISECONDS.toHours(Long.parseLong(jsonObject.get("startTime").toString())) ;
        long endTime = TimeUnit.MILLISECONDS.toHours(Long.parseLong(jsonObject.get("endTime").toString()));
        Date sDate = new Date(Long.parseLong(jsonObject.get("startTime").toString()));
        Date eDate = new Date(Long.parseLong(jsonObject.get("endTime").toString()));
        Activity activity = new Activity(Integer.parseInt(jsonObject.get("user_id").toString()),sDate,eDate,jsonObject.get("activity").toString());
        activity.setStatus("ok");
        activity.setTimeSpent(0);
        System.out.println(activity.toString());
        System.out.println(endTime-startTime);
        if(activity.getName().equals("Sleeping") && endTime-startTime >= 12){
            System.out.println("Prea mult a dormit patient-ul :" + activity.getUser_id()+". Nu a mai avut activitate de "+(endTime-startTime)+"ore");
            activity.setStatus("not ok");
            activity.setTimeSpent((int) (endTime-startTime));
        }else if(activity.getName().equals("Leaving") && endTime-startTime >= 12){
            System.out.println("Prea mult a stat afara patient-ul :" + activity.getUser_id()+". Nu a mai avut activitate de "+(endTime-startTime)+"ore");
            activity.setStatus("not ok");
            activity.setTimeSpent((int) (endTime-startTime));
        }else if((activity.getName().equals("Showering") || activity.getName().equals("Toileting")) && endTime-startTime >= 1){
            System.out.println("Prea mult a stat la baie patient-ul :" + activity.getUser_id()+". Nu a mai avut activitate de "+(endTime-startTime)+"ore");
            activity.setStatus("not ok");
            activity.setTimeSpent((int) (endTime-startTime));
        }
        this.activityRepository.save(activity);
    }
}
