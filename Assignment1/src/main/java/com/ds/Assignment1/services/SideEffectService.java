package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.SideEffectDTO;
import com.ds.Assignment1.dto.SideEffectViewDTO;
import com.ds.Assignment1.dto.builders.SideEffectBuilder;
import com.ds.Assignment1.dto.builders.SideEffectViewBuilder;
import com.ds.Assignment1.entities.SideEffect;
import com.ds.Assignment1.repositories.SideEffectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SideEffectService {

    private final SideEffectRepository sideEffectRepository;

    @Autowired
    public SideEffectService(SideEffectRepository sideEffectRepository) {
        this.sideEffectRepository = sideEffectRepository;
    }

    public SideEffectViewDTO findUserById(Integer id) {
        Optional<SideEffect> sideEffect = sideEffectRepository.findById(id);
        return SideEffectViewBuilder.generateDTOFromEntity(sideEffect.get());
    }

    public List<SideEffectViewDTO> findAll() {
        List<SideEffect> sideEffects = sideEffectRepository.getAllOrdered();
        return sideEffects.stream()
                .map(SideEffectViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(SideEffectDTO sideEffectDTO) {
        return sideEffectRepository
                .save(SideEffectBuilder.generateEntityFromDTO(sideEffectDTO))
                .getId();
    }

    public Integer update(SideEffectDTO sideEffectDTO) {
        return sideEffectRepository.save(SideEffectBuilder.generateEntityFromDTO(sideEffectDTO)).getId();
    }

    public void delete(SideEffectViewDTO sideEffectViewDTO) {
        this.sideEffectRepository.deleteById(sideEffectViewDTO.getId());
    }
}
