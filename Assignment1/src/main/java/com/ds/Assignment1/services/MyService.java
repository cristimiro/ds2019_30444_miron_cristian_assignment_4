package com.ds.Assignment1.services;

import com.ds.Assignment1.entities.Activity;
import com.ds.Assignment1.entities.MedicationPlan;
import com.ds.Assignment1.repositories.ActivityRepository;
import com.ds.Assignment1.repositories.MedicationPlanRepository;
import com.ds.Assignment1.repositories.PatientRepository;
import com.ds.Assignment1.soap.ActivityType;
import com.ds.Assignment1.soap.DoctorRequest;
import com.ds.Assignment1.soap.DoctorResponse;
import com.ds.Assignment1.soap.MedicationPlanType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MyService {

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    public DoctorResponse getAllRequests(DoctorRequest doctorRequest){
        DoctorResponse doctorResponse = new DoctorResponse();
        if(!doctorRequest.getMedicalPlanId().isEmpty()){
            try {
                Optional<MedicationPlan> optionalMedicationPlan = medicationPlanRepository.findById(Integer.parseInt(doctorRequest.getMedicalPlanId()));

                MedicationPlan medicationPlan = optionalMedicationPlan.get();

                MedicationPlanType medicationPlanType = new MedicationPlanType();

                medicationPlanType.setId(medicationPlan.getId().toString());
                medicationPlanType.setStartDate(medicationPlan.getStart_date().toString());
                medicationPlanType.setEndDate(medicationPlan.getEnd_date().toString());
                medicationPlanType.setPeriod(medicationPlan.getPeriod().toString());
                medicationPlanType.setStatus(medicationPlan.getStatus());

                doctorResponse.setMedicalPlan(medicationPlanType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            List<Activity> activityList = activityRepository.findAll();
            for(Activity activity : activityList){
                ActivityType activityType = new ActivityType();

                activityType.setId(activity.getId().toString());
                activityType.setEndTime(activity.getEndTime().toString());
                activityType.setName(activity.getName().toLowerCase());
                activityType.setStartTime(activity.getStartTime().toString());
                activityType.setStatus(activity.getStatus());
                activityType.setUserId(activity.getUser_id().toString());

                doctorResponse.getActivity().add(activityType);
            }
        }
        return doctorResponse;
    }
}
