package com.ds.Assignment1.services;


import com.ds.Assignment1.dto.MedicationDTO;
import com.ds.Assignment1.dto.MedicationViewDTO;
import com.ds.Assignment1.dto.builders.MedicationBuilder;
import com.ds.Assignment1.dto.builders.MedicationViewBuilder;
import com.ds.Assignment1.entities.Medication;
import com.ds.Assignment1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findUserById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationDTO> findAll() {
        List<Medication> medications = medicationRepository.getAllOrdered();
        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(Integer id) {
        this.medicationRepository.deleteById(id);
    }

}
