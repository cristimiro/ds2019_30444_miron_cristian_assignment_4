package com.ds.Assignment1.entities;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationPlan")

public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "medication_plan_medication",
            joinColumns = @JoinColumn(name = "medicationPlan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id"))
    private Set<Medication> medicationSet;


    @ManyToMany(mappedBy = "medicationPlanSet", fetch = FetchType.EAGER)
    private Set<Patient> patientSet;

    @Column(name="period")
    private Integer period;

    @Column(name="start_date")
    private Date start_date;

    @Column(name="end_date")
    private Date end_date;

    @Column(name="status")
    private String status;

    public MedicationPlan(Integer id, Integer period) {
        this.id = id;
        this.period = period;
    }

    public MedicationPlan(Integer id, Set<Medication> medicationSet, Integer period) {
        this.id = id;
        this.medicationSet = medicationSet;
        this.period = period;
    }

    public MedicationPlan(Integer id,Set<Medication> medicationSet, Integer period, Date start_date, Date end_date) {
        this.medicationSet = medicationSet;
        this.period = period;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public MedicationPlan(){

    }

    public Integer getPeriod() {
        return period;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Medication> getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(Set<Medication> medicationSet) {
        this.medicationSet = medicationSet;
    }

    public Integer getPeriods() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public void setPatientSet(Set<Patient> patientSet) {
        this.patientSet = patientSet;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "<html>MedicationPlan{" +"<br>" +
                "id=" + id +'\n' +"<br>" +
                ", medicationSet=" + medicationSet +"<br>" +
                ", patientSet=" + patientSet +"<br>" +
                ", period=" + period +"<br>" +
                ", start_date=" + start_date +"<br>" +
                ", end_date=" + end_date +"<br>" +
                ", status=" + status +"<br>" +
                "}</html>";
    }
}
