package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.CaregiverDTO;
import com.ds.Assignment1.dto.CaregiverViewDTO;
import com.ds.Assignment1.dto.PatientDTO;
import com.ds.Assignment1.entities.Caregiver;
import com.ds.Assignment1.entities.Patient;
import com.ds.Assignment1.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/caregivers")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService){
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public CaregiverDTO findById(@PathVariable("id") Integer id) {
        return caregiverService.findUserById(id) ;
    }

    @GetMapping()
    public List<CaregiverDTO> findAll() {
        return caregiverService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.insert(caregiverDTO);
    }

    @PostMapping(value = "/mapPatients")
    public void mapPatientCaregiver(@RequestBody Map<String, String> data) {
        caregiverService.mapPatient(data);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping(value="/{id}")
    public void delete(@PathVariable Integer id) {
        caregiverService.delete(id);
    }

//    @GetMapping(value="/{id}/patients")
//    public List<Patient> findByCaregiver(@PathVariable Integer id){
//        System.out.println(id);
//        return caregiverService.getPatients(id);
//    }
}
