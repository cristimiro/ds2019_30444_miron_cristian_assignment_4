package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.CaregiverDTO;
import com.ds.Assignment1.dto.DoctorDTO;
import com.ds.Assignment1.dto.PatientDTO;
import com.ds.Assignment1.entities.*;
import com.ds.Assignment1.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/login")
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping(value = "/{username}")
    public Object findByUsername(@PathVariable("username") String username){
        Object obj = loginService.findUser(username);
        if(obj instanceof Doctor){
            Doctor doctor = (Doctor)obj;
            return new DoctorDTO(doctor.getId(),doctor.getUsername(),doctor.getPassword(),doctor.getName(),doctor.getBirth_date(),doctor.getGender(),doctor.getAddress());
        } else if( obj instanceof  Patient){
            Patient patient = (Patient)obj;
            PatientDTO patientDTO = new PatientDTO(patient.getId(), patient.getUsername(), patient.getPassword(), patient.getName(), patient.getBirth_date(), patient.getGender(), patient.getAddress(), patient.getMedicationPlanSet());
            return patientDTO;
        } else {
            Caregiver caregiver = (Caregiver)obj;
            return new CaregiverDTO(caregiver.getId(), caregiver.getUsername(), caregiver.getPassword(), caregiver.getName(), caregiver.getBirth_date(), caregiver.getGender(), caregiver.getAddress(),caregiver.getPatientSet());
        }
    }

}
