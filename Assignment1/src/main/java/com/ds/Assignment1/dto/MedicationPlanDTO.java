package com.ds.Assignment1.dto;

import com.ds.Assignment1.entities.Medication;

import java.util.Date;
import java.util.Set;

public class MedicationPlanDTO {

    private Integer id;
    private Integer period;
    private Set<Medication> medicationSet;
    private Date start_date;
    private Date end_date;

    public MedicationPlanDTO(Integer id,  Integer period,Set<Medication> medicationSet, Date start_date, Date end_date) {
        this.id = id;
        this.period = period;
        this.medicationSet = medicationSet;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public MedicationPlanDTO(){

    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Set<Medication> getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(Set<Medication> medicationSet) {
        this.medicationSet = medicationSet;
    }
}
