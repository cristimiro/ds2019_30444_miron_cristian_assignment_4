package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.MedicationDTO;
import com.ds.Assignment1.dto.MedicationViewDTO;
import com.ds.Assignment1.entities.Medication;

public class MedicationViewBuilder {

    public static MedicationViewDTO generateDTOFromEntity(Medication medication){
        return new MedicationViewDTO(medication.getId(), medication.getName(), medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationViewDTO medicationViewDTO){
        return new Medication(medicationViewDTO.getId(),medicationViewDTO.getName(), medicationViewDTO.getDosage());
    }

}
