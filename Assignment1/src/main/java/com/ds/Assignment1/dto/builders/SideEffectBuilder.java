package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.SideEffectDTO;
import com.ds.Assignment1.dto.SideEffectViewDTO;
import com.ds.Assignment1.entities.SideEffect;

public class SideEffectBuilder {

    public SideEffectBuilder(){

    }

    public static SideEffectDTO generateDTOFromEntity(SideEffect sideEffect){
        return new SideEffectDTO(
                sideEffect.getId(),
                sideEffect.getSideEffect()
        );
    }

    public static SideEffect generateEntityFromDTO(SideEffectDTO sideEffectDTO){
        return new SideEffect(
                sideEffectDTO.getId(),
                sideEffectDTO.getSideEffect()
        );
    }

}
