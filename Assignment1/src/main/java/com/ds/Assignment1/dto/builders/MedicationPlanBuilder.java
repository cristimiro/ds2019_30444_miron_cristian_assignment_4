package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.MedicationPlanDTO;
import com.ds.Assignment1.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanDTO(
                medicationPlan.getId(), medicationPlan.getPeriod(), medicationPlan.getMedicationSet(), medicationPlan.getStart_date(), medicationPlan.getEnd_date()
        );
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO){
        return new MedicationPlan(
                medicationPlanDTO.getId(),medicationPlanDTO.getMedicationSet(),medicationPlanDTO.getPeriod(), medicationPlanDTO.getStart_date(), medicationPlanDTO.getEnd_date()
        );
    }

}
