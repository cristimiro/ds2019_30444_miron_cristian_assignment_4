package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.DoctorViewDTO;
import com.ds.Assignment1.entities.Doctor;

public class DoctorViewBuilder {

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(doctor.getId(), doctor.getUsername(), doctor.getName());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(doctorViewDTO.getId(),doctorViewDTO.getUsername(), doctorViewDTO.getName());
    }
}
